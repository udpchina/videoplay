
// VideoPlayDlg.h : header file
//
#include "helper_parsing.h"
#include "helper_Window.hpp"

#pragma once
#define METHOD_TCP 0
#define METHOD_UDP 1 

// CVideoPlayDlg dialog
class CVideoPlayDlg : public CDialogEx
{
// Construction
public:
	CVideoPlayDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_VIDEOPLAY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	int m_Method;
	CStreamWindow*	m_StreamingWindow;
	afx_msg void OnBnClickedBtnPlay();
};
