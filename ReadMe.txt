========================================================================
Code example: VideoPlay
========================================================================

==Description==
This example code demonstrates how to connect to UDP device and
play streams using SDK.

==Requirements==
1. You need first configure your development environment, please refer
to http://url.cn/KDFC4g 	    
2. This code requires NVC/IPE series SDK or IPX/IPN series SDK. Please 
download the SDK first.
 
==How to use==
1. You should put this project in {SDK folder}\SRC\netclient\starttools\ 
directory.
2. This project originally provides only Visual Studio 2010 project 
configuration. You can make your own building configurations, such as 
VS2003/VS2008/VS2012, etc.